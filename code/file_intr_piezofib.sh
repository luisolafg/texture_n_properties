# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

#compiling and running script for calculations in FORTRAN and drawing in Python
#this script is dual compatible; either on MinGW or proper POCIX system
#hence the ".exe" + "./" abiguity

echo ""
echo " Removing remains of previous runs"
rm piezofib.exe
rm BaTiO3_result.dat
rm *.o
echo " Compiling FORTRAN calculations code"

gfortran -c piezofib.f90
gfortran -c main_f_prog.f90
gfortran -o piezofib.exe *.o

echo " Running calculations in FORTRAN"
./piezofib.exe
echo " "
echo " Drawing property surface from Python"
python main_py_prog.py
