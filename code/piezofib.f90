! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this
! file, You can obtain one at http://mozilla.org/MPL/2.0/

Module piezofibcalc

    Implicit None

    REAL (KIND=8), DIMENSION (3,6)      :: gtexmat
    REAL (KIND=8)                       :: pi = 3.141592653589793D+00

    contains

    Subroutine gcalc

        REAL (KIND=8), DIMENSION (3, 6)  :: gmat
        REAL (KIND=8), DIMENSION (3,3,3) :: gtens
        REAL (KIND=8), DIMENSION (3,3)   :: Q
        REAL (KIND=8), DIMENSION (3,3,3) :: grotten, gtexten
        REAL (KIND=8)    :: phi1, phi, phi2, beta
        REAL (KIND=8)    :: a, b, c, d, dphi, dbeta, omeg, su, mh, mk, ml
        INTEGER (KIND=8) :: i, j, k, l, m, n, np, nphi, nb, nbeta
        INTEGER, PARAMETER :: out_unit = 13, in_unit = 12

        nphi = 360; nbeta = 720

        a = 0.d0; b = pi; c = 0.d0; d = 2.d0 * pi
        dphi = (b - a) / dble(nphi)
        dbeta = (d - c) / dble(nbeta)

        write(*,'(/A)')' Piezoelectricity for axial textures:'
        write(*,*)
        write(*,*) " Single crystal matrix:"

        OPEN(unit = in_unit, status = 'old',file = 'BaTiO3.dat', action = 'read')
            Do m = 1, 3
                read(in_unit,*) (gmat(m, i), i = 1, 6)
                write (*,'(6F10.2)')(gmat(m, i), i = 1, 6)
            End Do
        CLOSE(in_unit)

        write(*,'(/A$)')' Enter Gauss texture distribution Omega: '
        read(*,*) omeg

        call inte2D(omeg, su)

        ! Tensor diagonal elements
        Do i=1,3
            Do j=1,3
                gtens(i,j,j)=gmat(i,j)
            End Do
        End Do

        ! Off-diagonal tensor elements
        Do i=1,3
            gtens(i, 2, 3) = 0.5 * gmat(i, 4)
            gtens(i, 3, 2) = gtens(i, 2, 3)
            gtens(i, 1, 3) = 0.5 * gmat(i, 5)
            gtens(i, 3, 1) = gtens(i, 1, 3)
            gtens(i, 1, 2) = 0.5 * gmat(i, 6)
            gtens(i, 2, 1) = gtens(i, 1, 2)
        End Do

        phi1 = 0.d0   !Fibre texture, Bunge convention

        ! START THE GREAT LOOP FOR TENSOR COMPONENTS
        Do i = 1, 3
            Do j = 1, 3
                Do k = 1, 3
                    ! Setting to "0" the textured polycrystal piezotensor component:
                    gtexten(i, j, k) = 0.d0
                    ! loop on polar angle
                    Do np =1, nphi
                        phi = a + dphi/2.d0 + dble(np - 1)*dphi
                        Do nb = 1, nbeta   ! loop on azimuth
                                beta = c + dbeta/2.d0 + dble(nb - 1)*dbeta
                                phi2= (pi/2.d0) - beta    ! Bunge convention
                                Q(1,1) =  cos(phi2)* cos(phi1) - cos(phi)*sin(phi1)*sin(phi2)
                                Q(1,2) =  cos(phi2)* sin(phi1) + cos(phi)*cos(phi1)*sin(phi2)
                                Q(1,3) =  sin(phi2)* sin(phi)
                                Q(2,1) = -sin(phi2)* cos(phi1) - cos(phi)*sin(phi1)*cos(phi2)
                                Q(2,2) = -sin(phi2)* sin(phi1) + cos(phi)*cos(phi1)*cos(phi2)
                                Q(2,3) =  cos(phi2)* sin(phi)
                                Q(3,1) =  sin(phi) * sin(phi1)
                                Q(3,2) = -sin(phi) * cos(phi1)
                                Q(3,3) =  cos(phi)
                                Do l=1,3
                                    Do m = 1, 3
                                        Do n =1, 3
                                            grotten(i,j,k) =0.d0  ! Setting to "0" a rotated crystal piezotensor component:
                                        End Do
                                    End Do
                                End Do
                                Do l=1,3
                                    Do m = 1, 3
                                        Do n =1, 3
                                            grotten(i,j,k) = grotten(i,j,k) + Q(i,l)*Q(j,m)*Q(k,n)*gtens(l,m,n)
                                            !write(*,'(A)')' np,nb, l, m, n,    phi,     beta, grotten(i,j,k)'
                                        !write(*,'(5I3,3F10.3)') np,nb, l,m,n, 180*phi/pi,180*beta/pi, grotten(i,j,k)
                                        End Do
                                    End Do
                                End Do
                        !write(*,*)'i,k,j,grotten(i,j,k)', i,k,j,grotten(i,j,k)
                        gtexten(i,j,k) = gtexten(i,j,k)+rr(phi, beta, su, omeg)*grotten(i,j,k)*dphi*dbeta
                        End Do
                    End Do
                End Do
            End Do
        End Do

        Do i = 1, 3
                Do j = 1, 3
                    gtexmat(i,j)= gtexten(i,j,j)
                End Do
                gtexmat(i,4)=2.000*gtexten(i,2,3)
                gtexmat(i,5)=2.000*gtexten(i,1,3)
                gtexmat(i,6)=2.000*gtexten(i,1,2)
        End Do

        open (unit=out_unit,file="BaTiO3_result.dat",action="write",status="replace")
            Do i = 1,3
                write (out_unit,'(6F10.2)') (gtexmat(i,j), j =1,6)
            End Do
        close (out_unit)

    End Subroutine gcalc

    Subroutine consoleout
        INTEGER (KIND=8) :: i, j
        write(*,'(/A)') ' Textured polycrystal matrix:'
        Do i = 1,3
                write (*,'(6F10.2)') (gtexmat(i,j), j = 1,6)
        End Do
    End Subroutine consoleout

    Subroutine inte2D(omega, suma)
        implicit none
        real (kind = 8), intent (in out):: omega, suma
        real (kind = 8):: a, b, c, d, x, y, hx, hy, f
        integer (kind = 4) :: ii, jj, nx, ny

        nx = 1024; ny = 2048
        a = 0.d0; b = pi; c = 0.d0; d = 2.d0*pi

        hx = (b-a)/dble(nx); hy = (d-c)/dble(ny)
        suma = 0.0D+00
        Do ii = 1, nx
            x = a + hx/2.d0 + dble(ii-1)*hx
            Do jj = 1, ny
                y = c + hy/2.d0 + dble(jj-1)*hy
                suma = suma + hx * hy * func(x, y, omega)
            End Do
        End Do
        Return
    End Subroutine inte2D

    Function func(x, y, omega) result(f)
        real (kind = 8), intent(in) :: x, y, omega
        real (kind = 8)             :: f

        f = exp(-(180.d0*x/(pi*omega))**2) * sin(x)
    End Function func

    Function rr(phi, beta, su, omeg) result(r)
        real (kind = 8), intent(in) :: phi, beta, su, omeg
        real (kind = 8)             :: r

        r = (1.d0 / su) * exp( -(180.d0 * phi / (pi * omeg)) ** 2) * sin(phi)
    End Function rr


End Module piezofibcalc
