# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import numpy as np
from numpy import sin,cos,pi,abs
from mayavi import mlab

grotmat = np.loadtxt("BaTiO3_result.dat")

print "grotmat =", grotmat

# spherical coordinates

phi, beta = np.mgrid[0:1 * np.pi:181j, 0:2 * np.pi:361j]

x = sin(phi) * cos(beta)
y = sin(phi) * sin(beta)
z = cos(phi)

G1 = grotmat[0,0] * x * x * x + \
    (grotmat[0,1] + grotmat[1,5]) * x * y * y + \
    (grotmat[0,2] + grotmat[2,4]) * x * z * z + \
    (grotmat[0,3] + grotmat[1,4] + grotmat[2,5]) * x * y * z + \
    (grotmat[0,4] + grotmat[2,0]) * x * x * z + \
    (grotmat[0,5] + grotmat[1,0]) * x * x * y

G2 = grotmat[1,1] * y * y * y + \
    (grotmat[1,2] + grotmat[2,3]) * y * z * z + \
    (grotmat[1,3] + grotmat[2,1]) * y * y * z

G3 = grotmat[2,2] * z * z * z

G = G1 + G2 + G3

x1 = G * sin(phi) * cos(beta)
y1 = G * sin(phi) * sin(beta)
z1 = G * cos(phi)

#mlab.figure(bgcolor=(1,1,1),fgcolor=(0,0,0))
mlab.figure()
mlab.mesh(x1,y1,z1,scalars=np.abs(G))
mlab.scalarbar(orientation="vertical")
mlab.show()
